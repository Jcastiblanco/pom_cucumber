package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    protected static WebDriver driver;
    private static WebDriverWait wait;
    private static Actions action;

    static {
        ChromeOptions chromeOptions = new ChromeOptions();
        driver = new ChromeDriver(chromeOptions);
        wait = new WebDriverWait(driver, 10);
    }

    public BasePage(WebDriver driver) {
        BasePage.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public static void navigateTo(String url) {
        driver.get(url);
        // System.setProperty("webdriver.chrome.driver",
        // "src/test/resources/Chromedriver/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver",
                "C:/Users/jcastiblanco/OneDrive - DXC Production/Documents/Programas descargados/ChromeDriver/chromedirver v95/chromedriver.exe");
    }

    // ******************************************* */

    // DE ESTA MANERA MANERA SE DEJA UNA ESPERA
    // EXPLICITA DE MANERA EMBEBIDA EN CADA
    // BUSQUEDA QUE HAGAMOS

    private WebElement Find(String locator) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    // ******************************************** */

    // METODOS PARA INTERACCION CON TABLAS
    // METODO PARA TOMAT UN VALOR DE CUALQUIER CASILLA EN LA TABLA

    public String getValueFromTable(String locator, int row, int column) {
        String cellINeed = locator + "/table/tbody/tr[" + row + "]/td[" + column + "]";

        return Find(cellINeed).getText();
    }

    // METODO PARA SETEAR UN VALOR DE CUALQUIER CASILLA EN LA TABLA

    public void setValueOnTable(String locator, int row, int column, String stringToSend) {
        String cellToFill = locator + "/table/tbody/tr[" + row + "]/td[" + column + "]";

        Find(cellToFill).sendKeys(stringToSend);
    }

    // ******************************************** */

    public String getText(WebElement element) {
        return element.getText();
    }

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void type(String inputText, By locator) {
        driver.findElement(locator).sendKeys(inputText);
    }

    public static void closeBrowser() {
        driver.quit();
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public boolean isDisplayed(By locator) {
        try {
            return driver.findElement(locator).isDisplayed();

        } catch (org.openqa.selenium.NoSuchElementException e) {

            return false;
        }
    }

}
