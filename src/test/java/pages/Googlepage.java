package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Googlepage extends BasePage {

    private List<WebElement> bringMeAllElements(String searhResults2) {
        return null;
    }

    By searchlocator = By.id("search_query_top");
    private String searhResults = "name";

    public Googlepage() {
        super(driver);
    }

    public void navigateToGoogle() {
        navigateTo("https://www.falabella.com.co/falabella-co");
    }

    public void enterSearchCriteria(String state) throws InterruptedException {
        try {
            Thread.sleep(500);
            type(state, searchlocator);
        } catch (NoSuchElementException e) {
            System.out.println("The WebElement Search field couldn't be found");
            e.printStackTrace();
        }
    }

    public List<String> getAllSearchResults() {
        List<WebElement> list = bringMeAllElements(searhResults);
        List<String> stringsFromList = new ArrayList<String>();

        for (WebElement e : list) {
            stringsFromList.add(e.getText());
        }
        return stringsFromList;
    }
}
