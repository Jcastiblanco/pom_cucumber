package steps;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.Googlepage;

public class GoogleSteps {

    Googlepage list = new Googlepage();

    @Given("^I navigate to the list page$")
    public void navigateTotListPage() {
        list.navigateToGoogle();
    }

    @When("^I search (.*) in the list$")
    public void searchTheList(String state) throws InterruptedException {
        list.enterSearchCriteria(state);
    }

    @Then("^I can find the city in the list$")
    public void theCityIsThere() {
        List<String> lista = list.getAllSearchResults();

    }

}
